package control;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Set;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

import model.Heightmap;
import model.PaintScheme;
import model.Point;
import model.Triangle;

public class Renderer implements GLEventListener, KeyListener {

	public final double SCALE_XY = 100;
	public final double SCALE_Z = 10000;
	public final double KEY_SENSITIVITY = 15;
	public final double FLOW_IN_A_RIVER = 200;

	private boolean project3D;

	private PaintScheme paintScheme;

	private double eyeX, eyeY, eyeZ;
	private double centerX, centerY, centerZ;
	private double viewUpX, viewUpY, viewUpZ;

	private Heightmap heightmap;
	private FPSAnimator animator;

	private Set<Triangle> triangles;

	private int maxX;
	private int maxY;

	public Renderer(Heightmap heightmap, FPSAnimator animator) {
		super();
		this.heightmap = heightmap;
		this.animator = animator;
		this.maxX = heightmap.getMaxX();
		this.maxY = heightmap.getMaxY();
		this.triangles = heightmap.toTriangles();
		this.triangles.removeIf(t -> t.isUnderTheSea()); // Izbacit morsko dno

		this.eyeX = 300 * this.SCALE_XY;
		this.eyeY = -100 * this.SCALE_XY;
		this.eyeZ = 2 * this.SCALE_Z;
		this.centerX = 300 * this.SCALE_XY;
		this.centerY = 300 * this.SCALE_XY;
		this.centerZ = 0;
		this.viewUpX = 0;
		this.viewUpY = 0;
		this.viewUpZ = 1;
		this.project3D = true;
		this.animator.setFPS(10);
		this.paintScheme = PaintScheme.NICE;
	}

	public Heightmap getHeightmap() {
		return heightmap;
	}

	public PaintScheme getPaintScheme() {
		return paintScheme;
	}

	public void setPaintScheme(PaintScheme paintScheme) {
		this.paintScheme = paintScheme;
	}

	@Override
	public void init(GLAutoDrawable glad) {
		glad.setAutoSwapBufferMode(GLAutoDrawable.SCREEN_CHANGE_ACTION_ENABLED);
		GL2 gl = glad.getGL().getGL2();
		gl.glEnable(GL2.GL_DEPTH_TEST);
		gl.glDepthFunc(GL2.GL_LEQUAL);
		this.getHeightmap().normalize();
		this.animator.start();
	}

	@Override
	public void display(GLAutoDrawable glad) {
		// Uzmes si GL2 objekt
		GL2 gl = glad.getGL().getGL2();

		// Uzmes si i ovo ak te bas zanima
		int width = glad.getSurfaceWidth();
		int height = glad.getSurfaceHeight();

		// Postavljanje pozadine itd
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();

		// Crtanje
		if (this.project3D)
			this.renderScene3d(gl);
		else
			this.renderScene2d(gl);

		glad.swapBuffers();

	}

	@Override
	public void reshape(GLAutoDrawable glad, int x, int y, int width, int height) {
		// Uzmes si GL2 objekt
		GL2 gl = glad.getGL().getGL2();
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();

		if (this.project3D)
			projectionSetup3d(gl, width, height);
		else
			projectionSetup2d(gl, width, height);

		gl.glMatrixMode(GL2.GL_MODELVIEW);

		gl.glViewport(0, 0, width, height);

	}

	@Override
	public void dispose(GLAutoDrawable glad) {

	}

	private void projectionSetup3d(GL2 gl, int width, int height) {
		// gl.glFrustum(20, 20, 20, 20, 100, 1000); //Failed first try
		if (height <= 0)
			height = 1;
		final float h = (float) width / (float) height;

		GLU glu = new GLU();
		glu.gluPerspective(45.0f, h, 100 * this.SCALE_XY, 1500 * this.SCALE_XY);
	}

	private void projectionSetup2d(GL2 gl, int width, int height) {
		gl.glOrtho(0, width - 1, height - 1, 0, 0, 1);
	}

	private void renderScene2d(GL2 gl) {

		gl.glBegin(GL2.GL_POINTS);
		for (Point p : this.getHeightmap().getPoints()) {
			this.pickColor(gl, p);
			gl.glVertex2d(p.getX(), p.getY());
		}
		gl.glEnd();

	}

	private void renderScene3d(GL2 gl) {

		// Postavljanje ocista i gledista
		GLU glu = new GLU();
		glu.gluLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, viewUpX, viewUpY, viewUpZ);

		gl.glPushMatrix();
		gl.glScaled(this.SCALE_XY, this.SCALE_XY, this.SCALE_Z);

		gl.glBegin(GL2.GL_TRIANGLES);
		
		// More u dva poligona
		gl.glColor3f(0, 0, 1);
		
		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(this.maxX, 0, 0);
		gl.glVertex3d(this.maxX, this.maxY, 0);

		gl.glVertex3d(0, 0, 0);
		gl.glVertex3d(this.maxX, this.maxY, 0);
		gl.glVertex3d(0, this.maxY, 0);

		// Strelica za orijentaciju
		gl.glColor3f(1, 0, 0);
		
		gl.glVertex3d(this.maxX / 2, this.maxY, 1 / this.SCALE_Z);
		gl.glVertex3d(this.maxX / 2 - 50, this.maxY - 50, 1 / this.SCALE_Z);
		gl.glVertex3d(this.maxX / 2 + 50, this.maxY - 50, 1 / this.SCALE_Z);
		
		
		for (Triangle t : this.triangles) {

			putPoint3d(gl, t.getA());
			putPoint3d(gl, t.getB());
			putPoint3d(gl, t.getC());

		}
		
		gl.glPopMatrix();
		gl.glEnd();
	}

	private void putPoint3d(GL2 gl, Point p) {

		this.pickColor(gl, p);
		gl.glVertex3d(p.getX(), p.getY(), this.getHeightmap().getAltitudes().get(p));

	}

	private void pickColor(GL2 gl, Point p) {
		double h = this.getHeightmap().getAltitudes().get(p);
		if (this.getPaintScheme().equals(PaintScheme.NICE)) {
			if (h < 0) {
				gl.glColor3d(0, 0, 1 + h);
			} else if (this.getHeightmap().getFlow().get(p) > this.FLOW_IN_A_RIVER) {
				gl.glColor3d(0.6, 0.6, 1);
			} else if (h < 0.3) {
				gl.glColor3d(0, 1 - 2 * h, 0);
			} else if (h < 0.9) {
				gl.glColor3d(h, 0.5 * h, 0.2 * h);
			} else {
				gl.glColor3d(1, h, 0.8 * h);
			}
		} else if (this.getPaintScheme().equals(PaintScheme.DEBUG)) {
			if (h < 0) {
				gl.glColor3d(0, 0, 1 + h);
			} else if (this.getHeightmap().getFlow().get(p) > this.FLOW_IN_A_RIVER) {
				gl.glColor3d(0.6, 0.6, 1);
			} else {
				gl.glColor3d(h, 1, h);
			}
		} else if (this.getPaintScheme().equals(PaintScheme.BASIC)) {
			if (h < 0) {
				gl.glColor3d(0, 0, 1);
			} else if (this.getHeightmap().getFlow().get(p) > this.FLOW_IN_A_RIVER) {
				gl.glColor3d(0.6, 0.6, 1);
			} else {
				gl.glColor3d(0, 1, 0);
			}
		} else if (this.getPaintScheme().equals(PaintScheme.HILL)) {
			if (h < 0) {
				gl.glColor3d(0, 0, 1);
			} else if (this.getHeightmap().getFlow().get(p) > this.FLOW_IN_A_RIVER) {
				gl.glColor3d(0.6, 0.6, 1);
			} else {
				h = Math.floor(h * 10) / 10.0;
				gl.glColor3d(0, 1 - h / 2, 0);
			}
		} else if (this.getPaintScheme().equals(PaintScheme.MOUNTAIN)) {
			if (h < 0) {
				gl.glColor3d(0, 0, 1);
			} else if (this.getHeightmap().getFlow().get(p) > this.FLOW_IN_A_RIVER) {
				gl.glColor3d(0.6, 0.6, 1);
			} else if (h < 0.3) {
				h = Math.floor(h * 10) / 10.0;
				gl.glColor3d(0, 1 - h, 0);
			} else if (h < 0.9) {
				h = Math.floor(h * 10) / 10.0;
				gl.glColor3d(h, 0.5 * h, 0.2 * h);
			} else {
				gl.glColor3d(1, 0.9, 0.75);
			}
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		System.out.println("Button pressed: " + e.getKeyChar());
		if ("w".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeY += this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("s".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeY -= this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("a".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeX -= this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("d".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeX += this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("f".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeZ -= this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("r".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeZ += this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("i".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeY += 10 * this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("k".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeY -= 10 * this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("j".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeX -= 10 * this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("l".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeX += 10 * this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("č".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeZ -= 10 * this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if ("p".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeZ += 10 * this.KEY_SENSITIVITY * this.SCALE_XY;
		} else if (" ".equalsIgnoreCase(String.valueOf(e.getKeyChar()))) {
			this.eyeX = 300 * this.SCALE_XY;
			this.eyeY = 100 * this.SCALE_XY;
			this.eyeZ = 800 * this.SCALE_XY;
		} else {
			return;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
