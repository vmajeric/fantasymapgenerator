package control;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import model.Heightmap;
import model.Point;

//Good Shit:
// https://www.tutorialspoint.com/jogl/jogl_quick_guide.htm

public class Main {

	public static void main(String[] args) {

		// getting the capabilities object of GL2 profile
		final GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		GLCanvas glCanvas = new GLCanvas(capabilities);
		
		//Setting up the animator
		final FPSAnimator animator = new FPSAnimator(glCanvas, 30); 
		
		//Renderer r = new Renderer(getHeightmapFromFile("ErozionAdvanced"), animator);
		//Renderer r = new Renderer(modifyMap(), animator);
		Renderer r = new Renderer(makeMountainIslandMap(), animator);
		
		glCanvas.addGLEventListener(r);
		glCanvas.addKeyListener(r);
		glCanvas.setSize(1000, 1000);
		
		

		// creating frame
		final JFrame frame = new JFrame("FantasyMapGenerator");
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowevent) {
				frame.dispose();
				System.exit(0);
			}
		});
		// adding canvas to it
		frame.getContentPane().add(glCanvas);
		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setVisible(true);
		glCanvas.requestFocusInWindow();

	}

	private static Heightmap modifyMap() {
		Heightmap terrain =  Heightmap.deserialize("MountainIsland");
		
		terrain
		.calculateFlow();


		
		
		
		return terrain;
	}
	
	private static Heightmap makePlainsMap() {
		Heightmap terrain = new Heightmap(600,600);
		double noise = 0.1;
		terrain
		.addCone(new Point(0,0), -0.03)
		.addCone(new Point(0,599), -0.03)
		.addCone(new Point(599,599), -0.03)
		.addCone(new Point(599,0), -0.03)
		
		.addCone(new Point(123,178), -0.15)
		.addCone(new Point(427,465), -0.16)
		.addCone(new Point(598,194), -0.11)
		.addCone(new Point(107,492), -0.18)
		
		.addCone(new Point(300,300), 0.4)
		.addCone(new Point(250,295), 0.29)
		
		.raiseSpike(new Point(267,452), 250, 31)
		.raiseSpike(new Point(267,250), 250, 33)
		.raiseSpike(new Point(350,330), 250, 34)
		.raiseSpike(new Point(158,578), 250, 32)
		
		.raiseBubble(new Point(132,147), 120)
		.raiseBubble(new Point(75,372), 131)
		.raiseBubble(new Point(157,468), 142)

		.raiseBubble(new Point(347,143), 140)
		.raiseBubble(new Point(268,504), 187)

		.raiseBubble(new Point(528,91), 137)
		.raiseBubble(new Point(404,394), 161)
		.raiseBubble(new Point(517,541), 153)

		.raiseBubble(new Point(250,50), 90)
		.addNoise(noise).addNoise(noise).addNoise(noise)
		.raiseAll(100)
		.smoothen(3)
		.calculateFlow();
		
		return terrain;
		
	}
	
	
	private static Heightmap makeNoiseMap() {
		Heightmap terrain = new Heightmap(600,600);
		double noise=4;
		terrain
		.addCone(new Point (300,300), -0.02)
		.addNoise(noise).addNoise(noise).addNoise(noise)
		.addNoise(noise).addNoise(noise).addNoise(noise)
		.center()
		.erode(6, 4, 0)
		.smoothen(1)
		.calculateFlow();
		return terrain;
	}
	
	private static Heightmap makeNoiseIslandMap() {
		Heightmap terrain = new Heightmap(600,600);
		double noise=4;
		terrain
		.addCone(new Point (300,300), -1)
		.addNoise(noise).addNoise(noise).addNoise(noise)
		.addNoise(noise).addNoise(noise).addNoise(noise)
		.center()
		.calculateFlow();
		return terrain;
	}
	
	private static Heightmap makeMountainIslandMap() {
		Heightmap terrain = new Heightmap(600,600);
		System.out.println("Making terrain...");
		double noise=4;
		//terrain.raiseMountainRange(new Point(100,100), new Point(200,200), 20, 20).center();
		terrain
		.raiseMountain(new Point(200,200), 95, 275)
		.raiseMountain(new Point(150,279), 108, 253)
		.raiseMountain(new Point(345,180), 61, 225)
		.raiseMountain(new Point(350,327), 112, 175)
		.raiseBubble(new Point(275,250), 78)
		.raiseSpike(new Point (250,250),125,255)
		.raiseSpike(new Point (200,310),185,135)
		.raiseSpike(new Point (300,300),305,95)
		.addCone(new Point (275,275), -0.3)
		.addCone(new Point (59,215), -0.6)
		.addCone(new Point (198,125), -0.19)
		.addCone(new Point (563,511), -0.16)
		.addCone(new Point (148,437), -0.11)
		.addCone(new Point (439,217), -0.13)
		.addNoise(noise).addNoise(noise).addNoise(noise)
		.addNoise(noise).addNoise(noise).addNoise(noise)
		.smoothen(1)
		.raiseAll(305)
		//.serialize("NiceStart")
		.calculateFlow();
		
		 //Erode notes: n > 6*noise/koef; Maximalni Spike je cca. 2 * Maximalni Radijus * koef
		return terrain;
	}
	
	private static Heightmap getHeightmapFromFile(String name) {
		Heightmap terrain =  Heightmap.deserialize(name);
		terrain.calculateFlow();
		return terrain;
	}
	
}
