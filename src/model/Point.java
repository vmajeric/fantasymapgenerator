package model;

import java.io.Serializable;

import com.jogamp.opengl.GL2;

public class Point implements Serializable{

	private static final long serialVersionUID = -6024095790683859553L;
	private int x; // x coordinate
	private int y; // y coordinate
	//TODO vrati koordinate u int, ovo je preambiciozno zasad

	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public double distanceToPoint(Point p) {
		return (Math.sqrt(Math.pow(this.getX() - p.getX(), 2) + Math.pow(this.getY() - p.getY(), 2)));
	}

	public double distanceToLine(Point begin, Point end) {
		double a = end.getX() - begin.getX();
		double b = end.getY() - begin.getY();

		double t = (a * (this.getX() - begin.getX()) + b * (this.getY() - begin.getY())) / (a * a + b * b);
		return Math.sqrt(
				Math.pow(this.getX() - begin.getX() - a * t, 2) + Math.pow(this.getY() - begin.getY() - b * t, 2));

	}

	

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return (this.x + ", " + this.y);
	}
	
	

}
