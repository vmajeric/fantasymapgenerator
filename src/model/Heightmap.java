package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.Set;
import java.util.stream.Collectors;

import com.jogamp.opengl.GL2;

public class Heightmap implements Serializable {

	private static final long serialVersionUID = 2305810435652257115L;
	private Set<Point> points; 
	private Map<Point, Double> altitudes;
	private Map<Point, Double> flow;

	private int maxX;
	private int maxY;

	public final int SEALEVEL = Integer.MIN_VALUE;

	public Heightmap(int maxX, int maxY) { 
		super();

		this.maxX=maxX;
		this.maxY=maxY;
		
		this.points = new HashSet<>();
		this.altitudes = new HashMap<>();
		this.flow = new HashMap<>();
		for (int x = 0; x < maxX; x++)
			for (int y = 0; y < maxY; y++) {
				Point p = new Point(x, y);
				points.add(p);
				altitudes.put(p, 0.0);
				flow.put(p, 1.0);
			}
	}
	
	

	public Map<Point, Double> getAltitudes() {
		return altitudes;
	}

	public Map<Point, Double> getFlow() {
		return flow;
	}

	public Set<Point> getPoints() {
		return points;
	}

	

	public int getMaxX() {
		return maxX;
	}



	public int getMaxY() {
		return maxY;
	}



	public Heightmap raisePoint(Point p, double h) {
		this.setPoint(p, this.getAltitudes().get(p) + h);

		return this;
	}

	public Heightmap setPoint(Point p, double h) {
		if (this.getPoints().contains(p)) {
			this.getAltitudes().put(p, h);
		}
		return this;
	}

	public Heightmap raiseBubble(Point center, double r) {
		System.out.println("raiseBubble");
		this.getPoints().stream().filter(p -> p.distanceToPoint(center) < Math.abs(r)).forEach(p -> this.raisePoint(p,
				Math.signum(r) * Math.sqrt(Math.pow(r, 2) - Math.pow(p.distanceToPoint(center), 2))));
		return this;

	}

	public Heightmap raiseSpike(Point center, double r, double h) {
		System.out.println("raiseSpike");
		this.getPoints().stream().filter(p -> p.distanceToPoint(center) < Math.abs(r))
				.forEach(p -> this.raisePoint(p, h * Math.pow(1.0 - p.distanceToPoint(center) / Math.abs(r), 2.0)));
		return this;

	}

	

	public Heightmap raiseMountain(Point center, double r, double h) {
		System.out.println("raiseMountain");
		this.raiseBubble(center, Math.abs(r));
		this.raiseSpike(center, Math.abs(r), Math.abs(h));
		return this;
	}

	public Heightmap addNoise(double max) {
		System.out.println("noise");
		this.getPoints().stream().forEach(p -> raisePoint(p, max * Math.random()));
		return this;
	}

	public Heightmap smoothen(int n) {
		for (int i = 0; i < n; i++) {
			System.out.print((i + 1) + "/" + n + " Smoothing...");

			Map<Point, Double> temp = new HashMap<>();
			this.getPoints().stream().forEach(p -> temp.put(p, this.averageNeighbours(p, true)));
			this.altitudes = temp; // I know its not nice, but its faster
			System.out.println("Done!");
		}
		return this;
	}

	private Set<Point> findNeighbours(Point p, boolean includeP) {
		//TODO ovo zasad radi ali će trebat ponovo napisat kad/ako uvedem šum u raspored točaka
		int x = p.getX();
		int y = p.getY();

		Set<Point> set = new HashSet<>();
		if(includeP) set.add(p);

		set.add(new Point(x - 1, y));
		set.add(new Point(x + 1, y));
		set.add(new Point(x, y - 1));
		set.add(new Point(x, y + 1));

		set.add(new Point(x - 1, y - 1));
		set.add(new Point(x - 1, y + 1));
		set.add(new Point(x + 1, y - 1));
		set.add(new Point(x + 1, y + 1));

		set.retainAll(this.getPoints());
		return set;
	}

	private double averageNeighbours(Point p, boolean includeP) {

		return this.findNeighbours(p, includeP).stream().mapToDouble(this.getAltitudes()::get).average().getAsDouble();
	}

	public Heightmap normalize() {
		if (this.getAltitudes().keySet().isEmpty())
			return this;
		double max = this.getAltitudes().keySet().stream().mapToDouble(p -> Math.abs(this.getAltitudes().get(p))).max()
				.getAsDouble();
		this.radicalize(1 / max);
		return this;

	}

	public Heightmap radicalize(double k) {
		this.points.forEach(p -> this.getAltitudes().put(p, k * this.getAltitudes().get(p)));
		return this;
	}

	public Heightmap centerMedian() {
		double t = 0;
		double[] a = this.getAltitudes().keySet().stream().mapToDouble(p -> this.getAltitudes().get(p)).toArray();
		Arrays.sort(a); // INEFFICIENCY postoji quickselect algoritam, radi slicno ko quicksort i bio brzi
		if (a.length % 2 == 0)
			t = (a[a.length / 2 - 1] + a[a.length / 2]) / 2;
		else
			t = a[(a.length - 1) / 2];

		final double median = t;
		this.getPoints().forEach(p -> this.raisePoint(p, -1 * median));
		return this;
	}

	public Heightmap center() {
		if (this.getAltitudes().keySet().isEmpty())
			return this;
		double avg = this.getPoints().stream().mapToDouble(p -> this.getAltitudes().get(p)).average().getAsDouble();

		this.getAltitudes().keySet().forEach(p -> this.raisePoint(p, -1 * avg));
		return this;
	}

	public Heightmap raiseAll(double h) {

		this.getPoints().forEach(p -> this.raisePoint(p, h));

		return this;
	}

	public Heightmap addCone(Point center, double k) {
		System.out.println("addCone");

		this.getPoints().forEach(p -> this.raisePoint(p, k * p.distanceToPoint(center)));

		return this;
	}

	public Heightmap resetFlow() {
		this.getPoints().parallelStream().forEach(p -> this.getFlow().put(p, 1.0));
		return this;
	}

	private List<Point> getSortedPointList() {
		System.out.println("Sorting points...");
		return this.getPoints().stream().filter(p -> this.getAltitudes().get(p) > this.SEALEVEL)
				.sorted(Comparator.comparingDouble(this.getAltitudes()::get).reversed()).collect(Collectors.toList());

	}

	public Heightmap calculateFlow() {
		List<Point> templ = this.getSortedPointList();
		return this.calculateFlow(templ);
	}

	private Heightmap calculateFlow(List<Point> templ) {
		this.resetFlow();
		System.out.print("Calculating flow...");
		for (Point p : templ) {
			Set<Point> susjedi = this.findNeighbours(p, false);
			Point next = susjedi.stream().min(Comparator.comparing(this.getAltitudes()::get)).get();
			this.getFlow().put(next, this.getFlow().get(p) + this.getFlow().get(next));
		}
		System.out.println("Done!");
		return this;
	}

	public Heightmap erode(int n, double koef, double drill) {
		for (int i = 0; i < n; i++) {
			List<Point> templ = this.getSortedPointList();
			Set<Point> spikes = new HashSet<>();

			this.calculateFlow(templ);
			System.out.print((i + 1) + "/" + n + " Eroding...");
			for (Point p : templ) {
				Set<Point> susjedi = this.findNeighbours(p, true);
				Point next = susjedi.stream().min(Comparator.comparing(this.getAltitudes()::get)).get();
				if (p.equals(next)) {
					spikes.add(p);
				} else {
					double dh = (this.getAltitudes().get(p) - this.getAltitudes().get(next));
					this.raisePoint(p, -koef * (dh / Math.sqrt(dh * dh + 1)) * this.getFlow().get(p));
					this.raisePoint(next, (1 - drill) * koef * (dh / Math.sqrt(dh * dh + 1)) * this.getFlow().get(p));
				}
			}
			spikes.forEach(s -> this.setPoint(s, this.averageNeighbours(s, false)));
			System.out.println("Done!");
		}
		return this;
	}

	public Heightmap flattenSeaBottom(double d) {
		this.getPoints().parallelStream().filter(p -> this.getAltitudes().get(p) < d)
				.forEach(p -> this.getAltitudes().put(p, d));
		return this;
	}

	public Heightmap lowerSky(double d) {
		this.getPoints().parallelStream().filter(p -> this.getAltitudes().get(p) > d)
				.forEach(p -> this.getAltitudes().put(p, d));
		return this;
	}

	public Heightmap advancedErode(int n, double koef, double drill) {

		for (int i = 0; i < n; i++) {
			System.out.println((i + 1) + "/" + n + " Advanced Erode:");
			this.erode(1, koef, drill);
			this.smoothen(1);
		}

		return this;
	}

	public Heightmap combine(Heightmap other) {
		System.out.print("Combining...");
		Set<Point> temps = new HashSet<>();
		temps.addAll(this.getPoints());
		temps.retainAll(other.getPoints());
		temps.forEach(p -> this.raisePoint(p, other.getAltitudes().get(p)));
		System.out.println("Done!");

		return this;
	}

	public Set<Triangle> toTriangles() {// Kodirano ovak radi preglednosti
		System.out.print("Making triangles...");
		Set<Triangle> triangles = new HashSet<>();

		Set<Point> keySet = this.getAltitudes().keySet();

		for (Point p : keySet)
			if ((p.getX() + p.getY()) % 2 == 0) {
				Point a = new Point(p.getX(), p.getY() - 1);
				Point b = new Point(p.getX() + 1, p.getY() - 1);
				Point c = new Point(p.getX() + 1, p.getY());
				Point d = new Point(p.getX() + 1, p.getY() + 1);
				Point e = new Point(p.getX(), p.getY() + 1);
				if (keySet.contains(a) && keySet.contains(b))
					triangles.add(new Triangle(p, a, b));
				if (keySet.contains(b) && keySet.contains(c))
					triangles.add(new Triangle(p, b, c));
				if (keySet.contains(c) && keySet.contains(d))
					triangles.add(new Triangle(p, c, d));
				if (keySet.contains(d) && keySet.contains(e))
					triangles.add(new Triangle(p, d, e));
			}
		triangles.forEach(t -> t.setUnderTheSea(this.getAltitudes().get(t.getA()) < 0
				&& this.getAltitudes().get(t.getB()) < 0 && this.getAltitudes().get(t.getC()) < 0));

		System.out.println("Done!");
		return triangles;
	}

	public Heightmap serialize(String name) {

		try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("./dat/" + name + ".dat"))) {
			System.out.print("Serializing " + name + "...");

			out.writeObject(this);

		} catch (IOException ex) {
			System.err.println(ex);
		}
		System.out.println("Done!");
		return this;
	}

	public static Heightmap deserialize(String name) {

		File f = new File("./dat/" + name + ".dat");

		if (f.exists()) {
			try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(f.getPath()))) {
				System.out.print("Deserializing...");
				Heightmap res = (Heightmap) in.readObject();
				System.out.println("Done!");
				return res;
			} catch (IOException ex) {
				System.err.println(ex);
			} catch (ClassNotFoundException e) {
				System.err.println(e);
			}
		}

		System.out.println("Failed.");
		return null;
	}
	
	private Color pickColor(PaintScheme ps, Point p, double riverFlow) {//This will cause problems if heightmap isn't normalized
		
		double h = this.getAltitudes().get(p);
		Color r=new Color();
		if (ps.equals(PaintScheme.NICE)) {
			if (h < 0) {
				r.setRGB(0, 0,  1 + h);
			} else if (this.getFlow().get(p) > riverFlow) {
				r.setRGB(0.6, 0.6, 1);
			} else if (h < 0.3) {
				r.setRGB( 0, 1 - 2 * h, 0);
			} else if (h < 0.9) {
				r.setRGB( h, 0.5 * h, 0.2 * h);
			} else {
				r.setRGB(1,h, 0.8 * h);
			}
		} else if (ps.equals(PaintScheme.DEBUG)) {
			if (h < 0) {
				r.setRGB(0, 0, 1 + h);
			} else if (this.getFlow().get(p) > riverFlow) {
				r.setRGB(0.6, 0.6, 1);
			} else {
				r.setRGB(h, 1, h);
			}
		} else if (ps.equals(PaintScheme.BASIC)) {
			if (h < 0) {
				r.setRGB(0, 0, 1);
			} else if (this.getFlow().get(p) > riverFlow) {
				r.setRGB(0.6, 0.6, 1);
			} else {
				r.setRGB(0, 1, 0);
			}
		} else if (ps.equals(PaintScheme.HILL)) {
			if (h < 0) {
				r.setRGB(0, 0, 1);
			} else if (this.getFlow().get(p) > riverFlow) {
				r.setRGB(0.6, 0.6, 1);
			} else {
				h = Math.floor(h * 10) / 10.0;
				r.setRGB(0, 1 - h / 2, 0);
			}
		} else if (ps.equals(PaintScheme.MOUNTAIN)) {
			if (h < 0) {
				r.setRGB(0, 0, 1);
			} else if (this.getFlow().get(p) > riverFlow) {
				r.setRGB(0.6, 0.6, 1);
			} else if (h < 0.3) {
				h = Math.floor(h * 10) / 10.0;
				r.setRGB(0, 1 - h, 0);
			} else if (h < 0.9) {
				h = Math.floor(h * 10) / 10.0;
				r.setRGB(h, 0.5 * h, 0.2 * h);
			} else {
				r.setRGB(1, 0.9, 0.75);
			}
		}
		return r;
	}

}
