package model;

import java.util.HashSet;
import java.util.Set;

public class Triangle {
	private Point a;
	private Point b;
	private Point c;

	private boolean underTheSea = false;

	public Triangle(Point a, Point b, Point c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public Point getA() {
		return a;
	}

	public void setA(Point a) {
		this.a = a;
	}

	public Point getB() {
		return b;
	}

	public void setB(Point b) {
		this.b = b;
	}

	public Point getC() {
		return c;
	}

	public void setC(Point c) {
		this.c = c;
	}

	public boolean isUnderTheSea() {
		return underTheSea;
	}

	public void setUnderTheSea(boolean underTheSea) {
		this.underTheSea = underTheSea;
	}

	@Override
	public int hashCode() {
		Set<Point> set = new HashSet<>();
		set.add(this.getA());
		set.add(this.getB());
		set.add(this.getC());
		;
		final int prime = 31;
		int result = 1;
		result = prime * result + ((set == null) ? 0 : set.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Triangle other = (Triangle) obj;
		Set<Point> set = new HashSet<>();
		set.add(this.getA());
		set.add(this.getB());
		set.add(this.getC());
		Set<Point> otherSet = new HashSet<>();
		otherSet.add(other.getA());
		otherSet.add(other.getB());
		otherSet.add(other.getC());

		if (!set.equals(otherSet))
			return false;
		return true;
	}

}
