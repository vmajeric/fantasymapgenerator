package model;

public class Color {
	private float r;
	private float g;
	private float b;
	private float a;
	
	public Color() {
		super();
		this.r = 0;
		this.g = 0;
		this.b = 0;
		this.a = 1;
	}
	
	public Color(float r, float g, float b) {
		super();
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = 1;
	}
	
	public Color(float r, float g, float b, float a) {
		super();
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}

	public float getR() {
		return r;
	}

	public float getG() {
		return g;
	}

	public float getB() {
		return b;
	}

	public float getA() {
		return a;
	}

	public void setR(float r) {
		this.r = r;
	}

	public void setG(float g) {
		this.g = g;
	}

	public void setB(float b) {
		this.b = b;
	}

	public void setA(float a) {
		this.a = a;
	}
	
	public void setRGB(float r, float g, float b) {
		this.r=r;
		this.g=g;
		this.b=b;
	}
	
	public void setRGB(double r, double g, double b) {
		this.r=(float)r;
		this.g=(float)g;
		this.b=(float)b;
	}
	
	public void setRGBA(float r, float g, float b, float a) {
		this.r=r;
		this.g=g;
		this.b=b;
		this.a=a;
	}
	
	public void setRGBA(double r, double g, double b, double a) {
		this.r=(float)r;
		this.g=(float)g;
		this.b=(float)b;
		this.a=(float)a;
	}
}
